function doThing() {
        console.log("Name:", this.name);
}

a = {name: "Joe", doThing: doThing};
b = {name: "Bill", doThing: doThing};

a.doThing(); // Name: Joe
b.doThing(); // Name: Bill
doThing(); // Name: undefined